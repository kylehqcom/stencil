package stencil

// Error is the Stencil package error type
type Error struct {
	s string
}

var (
	// ErrDuplicateOnLoad is returned when a duplicate template is found on load
	ErrDuplicateOnLoad = NewStencilError("Duplicate template found on load")

	// ErrEmptyMatchPattern is returned when an invalid empty pattern is passed to match
	ErrEmptyMatchPattern = NewStencilError("Empty pattern to match")

	// ErrNoMatch is returned when no match is found
	ErrNoMatch = NewStencilError("No match")

	// ErrLoadTimeout is returned when loading of templates exceeds allowable limit
	ErrLoadTimeout = NewStencilError("Load timeout breached")
)

// Error will return the string representation of a StencilError
func (e Error) Error() string {
	return e.s
}

// IsStencilError will return true on Error Stencil type
func IsStencilError(err error) bool {
	_, ok := err.(*Error)
	return ok
}

// NewStencilError will create a new Error Stencil type error
func NewStencilError(errorMessage string) error {
	return &Error{errorMessage}
}
