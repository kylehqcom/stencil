package executor

import (
	"html/template"
	"io"
)

type (
	// Executor is the interface used to "execute" templates
	Executor interface {

		// Execute follows the Go packages Execute method for ease of use
		Execute(wr io.Writer, t *template.Template, data interface{}) error
	}

	// Option defines the behaviours for executing
	Option func(*Options)

	// Options holds options to alter this executors behaviour. Currently a placeholder.
	Options struct {
	}
)

// NewOptions will return a new ExecutorOptions instance with sensible defaults
func NewOptions() *Options {
	return &Options{}
}
