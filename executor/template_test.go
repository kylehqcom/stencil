package executor_test

import (
	"bytes"
	"fmt"
	"html/template"
	"testing"

	"gitlab.com/kylehqcom/stencil/executor"
)

func TestNewExecutor(t *testing.T) {
	t.Parallel()
	e := executor.NewTemplateExecutor()
	if fmt.Sprintf("%T", e) != "*executor.TemplateExecutor" {
		t.Error("Expected a executor.TemplateExecutor pointer instance")
	}

	var i interface{} = e
	_, ok := i.(executor.Executor)
	if !ok {
		t.Error("Assert executor implements executor.TemplateExecutor interface")
	}
}

func TestExecute(t *testing.T) {
	t.Parallel()
	tm := template.New("")
	tm.Funcs(template.FuncMap{
		"helper": func() string { return "helped" },
	})
	tm, err := tm.Parse("Help me: {{helper}}")
	if err != nil {
		t.Error(fmt.Sprintf("Unexpected error: %s", err))
	}

	e := executor.NewTemplateExecutor()
	buf := bytes.NewBuffer(nil)
	err = e.Execute(buf, tm, nil)
	if err != nil {
		t.Error(fmt.Sprintf("Unexpected error: %s", err))
	}

	expected := "Help me: helped"
	if expected != buf.String() {
		t.Error(fmt.Sprintf("Expected \"%s\" but got \"%s\"", expected, buf.String()))
	}
}
