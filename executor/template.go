package executor

import (
	"html/template"
	"io"
)

type (
	// TemplateExecutor is this packages default executor and implements the Executor interface
	TemplateExecutor struct {
		Options *Options
	}
)

// Ensure the TemplateExecutor implements the Executor interface
var _ Executor = &TemplateExecutor{}

// NewTemplateExecutor will return a new instance to this packages default executor
func NewTemplateExecutor(opts ...Option) *TemplateExecutor {
	e := &TemplateExecutor{
		Options: NewOptions(),
	}
	for _, opt := range opts {
		opt(e.Options)
	}

	return e
}

// Execute implements the Executor interface and just calls the underlying template.Execute
func (e *TemplateExecutor) Execute(wr io.Writer, t *template.Template, data interface{}) error {
	return t.Execute(wr, data)
}
