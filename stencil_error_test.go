package stencil_test

import (
	"fmt"
	"testing"

	"gitlab.com/kylehqcom/stencil"
)

func TestError(t *testing.T) {
	errMsg := "test error msg"
	e := stencil.NewStencilError(errMsg)

	if fmt.Sprintf("%T", e) != "*stencil.Error" {
		t.Error("Expected NewStencilError to return stencil.Error type")
	}

	if !stencil.IsStencilError(e) {
		t.Error("Expected stencil error type")
	}

	if errMsg != e.Error() {
		t.Error("Expected stencil error message to be identical")
	}
}
