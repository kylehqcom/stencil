package loader

import (
	"html/template"
	"io/ioutil"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/kylehqcom/stencil"
)

type (
	// Filepath is this packages default loader and implements the Loader interface
	Filepath struct {
		funcMap template.FuncMap
		Options *Options
		paths   []string
	}
)

// Ensure the Filepath implements the Loader interface
var _ Loader = &Filepath{}

// NewFilepathLoader will return a new instance this packages default filepath loader
func NewFilepathLoader(paths []string, opts ...Option) *Filepath {
	loaderOpts := NewOptions()
	for _, opt := range opts {
		opt(loaderOpts)
	}

	return &Filepath{
		funcMap: template.FuncMap{},
		Options: loaderOpts,
		paths:   paths,
	}
}

// Load will load and parse your template source returning a Collection/error tuple
func (l *Filepath) Load() (*Collection, error) {
	c := &Collection{}
	c.T = template.New("").Funcs(l.funcMap)

	wg := sync.WaitGroup{}
	tplChan := make(chan *template.Template)
	errChan := make(chan error)

	for _, path := range l.paths {
		matches, err := filepath.Glob(path)
		if err != nil {
			return c, err
		}

		wg.Add(len(matches))

		for _, match := range matches {
			go func(m string, tc chan *template.Template, ec chan error) {
				b, err := ioutil.ReadFile(m)
				if err != nil {
					ec <- err
					return
				}

				if !l.Options.AllowDuplicates {
					if exists := c.T.Lookup(m); exists != nil {
						ec <- stencil.ErrDuplicateOnLoad
						return
					}
				}

				t := c.T.New(m)
				if l.Options.MustParse {
					t = template.Must(t.Parse(string(b)))
					tc <- t
					return
				}

				t, err = t.Parse(string(b))
				if err != nil {
					ec <- err
					return
				}
				tc <- t
			}(match, tplChan, errChan)
		}
	}

	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()

loop:
	// Loop over all matches. We return on any error, and return on l.Options.LoadTimeout breach
	for {
		select {
		case err := <-errChan:
			return c, err
		case t := <-tplChan:
			c.T, _ = c.T.AddParseTree(t.Name(), t.Tree)
			wg.Done()
		case <-time.After(l.Options.LoadTimeout): // Used more as a safety to break from infinite loop
			return c, stencil.ErrLoadTimeout
		case <-done:
			break loop
		}
	}
	return c, nil
}

// RegisterFuncs will register template funcs to this Loader collection
func (l *Filepath) RegisterFuncs(fnMap template.FuncMap) {
	l.funcMap = fnMap
}
