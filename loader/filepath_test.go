package loader_test

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/loader"
)

func TestNewLoader(t *testing.T) {
	t.Parallel()
	l := loader.NewFilepathLoader([]string{})
	if fmt.Sprintf("%T", l) != "*loader.Filepath" {
		t.Error("Expected a Filepath pointer instance")
	}

	var i interface{} = l
	_, ok := i.(loader.Loader)
	if !ok {
		t.Error("Assert StencilLoader implements stencil.Loader interface")
	}

	if l.Options.AllowDuplicates {
		t.Error("Expected allow duplicates to be false by default")
	}
}

func TestLoad(t *testing.T) {
	t.Parallel()
	l := loader.NewFilepathLoader([]string{"\\"}, loader.WithMustParse(true))
	_, err := l.Load()
	if err == nil {
		t.Error("Expected syntax error in pattern")
	}

	wd, _ := os.Getwd()
	tpl1Path := filepath.Join(wd, "../data/fixtures/fixture1.html")
	tpl2Path := filepath.Join(wd, "../data/fixtures/fixture2.html")

	paths := []string{tpl1Path}
	l = loader.NewFilepathLoader(paths, loader.WithMustParse(true))
	collection, err := l.Load()
	if err != nil {
		t.Error("Expected no errors, got " + err.Error())
		return
	}

	if 2 != len(collection.T.Templates()) {
		t.Error(fmt.Sprintf("Expected 2 loaded template, got %d", len(collection.T.Templates())))
		t.FailNow()
	}

	// Add the 2 tpl which has a {{render}} method to confirm register funcs
	paths = append(paths, tpl2Path)
	l = loader.NewFilepathLoader(paths)
	collection, err = l.Load()
	if err == nil {
		t.Error("Expected syntax error in pattern")
	}
	expectedErrMsg := fmt.Sprintf(`template: %s:10: function "render" not defined`, tpl2Path)
	if err.Error() != expectedErrMsg {
		t.Error(fmt.Sprintf("Expected %s, got: %s", expectedErrMsg, err.Error()))
	}

	// Register the funcs and reload with error
	l.RegisterFuncs(template.FuncMap{
		"render": func() string { return "" },
	})
	collection, err = l.Load()
	if err != nil {
		t.Error("Expected no errors after funcs registered")
	}

	// Create a duplicate to confirm load error
	paths = append(paths, tpl1Path)
	l = loader.NewFilepathLoader(paths, loader.WithMustParse(true))
	l.RegisterFuncs(template.FuncMap{
		"render": func() string { return "" },
	})

	collection, err = l.Load()
	if err != stencil.ErrDuplicateOnLoad {
		t.Error("Expected duplicate errors on load")
	}
}
