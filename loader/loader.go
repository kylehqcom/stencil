package loader

import (
	"html/template"
	"sync"
	"time"
)

type (
	// Collection contains loaded and parsed templates, returned on Loader.Yield()
	Collection struct {
		sync.Mutex
		T *template.Template
	}

	// Loader is the interface used to "load" templates
	Loader interface {

		// Load will load and parse your template source returning a Collection/error tuple
		Load() (*Collection, error)

		// RegisterFuncs is used to bind funcs to your templates prior to loading. As the
		// Load method parses your templates, it may be necessary to register placeholder funcs.
		// These can optionally be replaced at runtime as required and specified by the Go stdLib
		// template.FuncMap. Refer template.Funcs(funcMap FuncMap) *Template
		RegisterFuncs(template.FuncMap)
	}

	// Option defines the behaviours for loading
	Option func(*Options)

	// Options are all defined behaviours for loading
	Options struct {

		// AllowDuplicates defines whether an error should be returned if duplicate found
		AllowDuplicates bool

		// LoadTimeout is used to return error on timeout duration is exceeded
		LoadTimeout time.Duration

		// MustParse if true, will panic on parse error
		MustParse bool
	}
)

func (Collection) New() *Collection {
	return &Collection{T: template.New("")}
}

// NewOptions will return a new loader options pointer
func NewOptions() *Options {
	return &Options{
		LoadTimeout: time.Second * 5,
	}
}

// WithAllowDuplicates by default, will error if a duplicate template name is loaded
func WithAllowDuplicates(allow bool) Option {
	return func(o *Options) {
		o.AllowDuplicates = allow
	}
}

// WithLoadTimeout allows you to define a duration to error if breached
func WithLoadTimeout(timout time.Duration) Option {
	return func(o *Options) {
		o.LoadTimeout = timout
	}
}

// WithMustParse if set to true will panic, otherwise will return error on template parse error
func WithMustParse(must bool) Option {
	return func(o *Options) {
		o.MustParse = must
	}
}
