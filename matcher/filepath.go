package matcher

import (
	"html/template"
	"strings"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/loader"
)

type (
	// Filepath is this packages default matcher and implements the Matcher interface
	Filepath struct {
		Options *Options
	}
)

// Ensure the Filepath implements the Matcher interface
var _ Matcher = &Filepath{}

// NewFilepathMatcher will return a new instance to this packages default matcher
func NewFilepathMatcher(opts ...Option) *Filepath {
	f := &Filepath{
		Options: NewOptions(),
	}
	for _, opt := range opts {
		opt(f.Options)
	}

	return f
}

// Match will match and return a template in the Collection if able or error
func (f *Filepath) Match(c *loader.Collection, pattern string) (*template.Template, error) {
	sep := f.Options.PathSeparator
	trimmed := strings.TrimLeft(pattern, sep)
	if "" == trimmed {
		return nil, stencil.ErrEmptyMatchPattern
	}

	parse := func(root, loc, pat string) string {
		root = strings.TrimRight(root, sep)
		loc = strings.Trim(loc, sep)
		if loc != "" {
			pat = loc + sep + pat
		}
		if root != "" {
			pat = root + sep + pat
		}
		return pat
	}

	// Create all the potential matches array in order of higher specificity first
	matches := [...]string{
		// root/locale/pattern
		parse(f.Options.PathRoot, f.Options.Locale, trimmed),

		// root/fallback_locale/pattern
		parse(f.Options.PathRoot, f.Options.FallbackLocale, trimmed),

		// root/pattern
		parse(f.Options.PathRoot, "", trimmed),

		// pattern
		parse("", "", pattern),
	}

	for _, match := range matches {
		if t := c.T.Lookup(match); t != nil {
			// We return a clone so callers always get a non executed fresh instance on match
			return t.Clone()
		}
	}

	return nil, stencil.ErrNoMatch
}
