package matcher_test

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

func TestNewMatcher(t *testing.T) {
	t.Parallel()
	m := matcher.NewFilepathMatcher()
	if fmt.Sprintf("%T", m) != "*matcher.Filepath" {
		t.Error("Expected a Filepath pointer instance")
	}

	var i interface{} = m
	_, ok := i.(matcher.Matcher)
	if !ok {
		t.Error("Assert StenMatcher implements stencil.Matcher interface")
	}
}

func TestMatchWithLocaleFallback(t *testing.T) {
	t.Parallel()
	wd, _ := os.Getwd()
	rootPath := filepath.Join(wd, "../data/fixtures/locale/")
	fixtures := []string{
		filepath.Join(rootPath, "en/index.html"),
		filepath.Join(rootPath, "es/home.html"),
	}

	// Create the collection for a loader
	pl := loader.NewFilepathLoader(fixtures)
	c, err := pl.Load()
	if err != nil {
		t.Error("Unable to load fixtures", err)
	}

	tests := []struct {
		content string
		err     error
		locale  string
		pattern string
	}{
		{
			err: stencil.ErrEmptyMatchPattern,
		},
		{
			err:     stencil.ErrEmptyMatchPattern,
			pattern: "/",
		},
		{
			err:     stencil.ErrNoMatch,
			pattern: "no match",
		},
		{
			content: "es/home.html",
			pattern: "es/home.html",
		},
		{
			content: "es/home.html",
			locale:  "es",
			pattern: "home.html",
		},
		{
			content: "en/index.html",
			locale:  "en",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			locale:  "es",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			locale:  "de",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			pattern: "en/index.html",
		},
		{
			content: "en/index.html",
			locale:  "es",
			pattern: "en/index.html",
		},
	}

	for _, tt := range tests {
		// Assign the matcher config test options
		m := matcher.NewFilepathMatcher(
			matcher.WithLocale(tt.locale),
			matcher.WithFallbackLocale("en"),
			matcher.WithFilepathSeparator("/"),
			matcher.WithPathRoot(rootPath),
		)
		res, err := m.Match(c, tt.pattern)
		if tt.err != err {
			t.Error(fmt.Sprintf("Got %s error but expected: %s", err, tt.err))
		}

		// Also check that the expected content, proving fallback
		if tt.content != "" {
			if res == nil {
				t.Error(fmt.Sprintf("Got nil content but expected: %s", tt.content))
			} else {
				buf := bytes.NewBuffer(nil)
				err := res.Execute(buf, nil)
				if err != nil {
					t.Error("Error on template execute", err)
				}
				if buf.String() != tt.content {
					t.Error(fmt.Sprintf("Got %s content but expected: %s", err, tt.content))
				}
			}
		}
	}
}

func TestMatchWithPathRoot(t *testing.T) {
	t.Parallel()
	wd, _ := os.Getwd()
	rootPath := filepath.Join(wd, "../data/fixtures/locale/")
	fixtures := []string{
		filepath.Join(rootPath, "/index.html"),
		filepath.Join(rootPath, "/en/index.html"),
		filepath.Join(rootPath, "/es/index.html"),
		filepath.Join(rootPath, "/es/home.html"),
	}

	// Create the collection for a loader
	pl := loader.NewFilepathLoader(fixtures)
	c, err := pl.Load()
	if err != nil {
		t.Error("Unable to load fixtures", err)
	}

	tests := []struct {
		content string
		err     error
		locale  string
		pattern string
	}{
		{
			err: stencil.ErrEmptyMatchPattern,
		},
		{
			err:     stencil.ErrEmptyMatchPattern,
			pattern: "/",
		},
		{
			err:     stencil.ErrNoMatch,
			pattern: "no match",
		},
		{
			content: "index.html",
			pattern: "index.html",
		},
		{
			content: "index.html",
			pattern: "/index.html",
		},
		{
			content: "en/index.html",
			locale:  "en",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			locale:  "/en/",
			pattern: "/index.html",
		},
		{
			content: "es/index.html",
			locale:  "es",
			pattern: "index.html",
		},
		{
			content: "index.html",
			locale:  "de",
			pattern: "index.html",
		},
		{
			content: "en/index.html",
			pattern: "en/index.html",
		},
		{
			content: "es/home.html",
			locale:  "es",
			pattern: "home.html",
		},
		{
			content: "es/home.html",
			pattern: "es/home.html",
		},
		{
			err:     stencil.ErrNoMatch,
			locale:  "en",
			pattern: "home.html",
		},
		{
			err:     stencil.ErrNoMatch,
			pattern: "en/home.html",
		},
		{
			content: "en/index.html",
			locale:  "es",
			pattern: rootPath + "/en/index.html",
		},
	}

	for _, tt := range tests {
		// Assign the matcher config test options
		m := matcher.NewFilepathMatcher(matcher.WithLocale(tt.locale), matcher.WithPathRoot(rootPath), matcher.WithFilepathSeparator("/"))
		res, err := m.Match(c, tt.pattern)
		if tt.err != err {
			t.Error(fmt.Sprintf("Got %s error but expected: %s", err, tt.err))
		}

		// Also check that the expected content, proving fallback
		if tt.content != "" {
			if res == nil {
				t.Error(fmt.Sprintf("Got nil content but expected: %s", tt.content))
			} else {
				buf := bytes.NewBuffer(nil)
				err := res.Execute(buf, nil)
				if err != nil {
					t.Error("Error on template execute", err)
				}
				if buf.String() != tt.content {
					t.Error(fmt.Sprintf("Got %s content but expected: %s", err, tt.content))
				}
			}
		}
	}
}
