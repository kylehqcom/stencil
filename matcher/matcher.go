package matcher

import (
	"html/template"
	"path/filepath"

	"gitlab.com/kylehqcom/stencil/loader"
)

type (
	// Matcher is the interface used to "match" templates
	Matcher interface {

		// Match will match any pattern string you provide an return a template pointer or error
		Match(c *loader.Collection, pattern string) (*template.Template, error)
	}

	// Option is the func type to apply options
	Option func(*Options)

	// Options are all defined behaviours for matching
	Options struct {

		// PathRoot if set, will be used to prefix all match patterns
		PathRoot string

		// PathSeparator is the Separator string used to join match patterns. Defaults to filepath.Separator
		PathSeparator string

		// Locale is the string for the current match
		Locale string

		// FallbackLocale is used if no match is made on the current Locale match
		FallbackLocale string
	}
)

// NewOptions will return a new matcher options pointer
func NewOptions() *Options {
	return &Options{
		PathSeparator: string(filepath.Separator),
	}
}

// WithPathRoot will set the root path value that will be prefixed to the name pattern on matching
func WithPathRoot(pathRoot string) Option {
	return func(o *Options) {
		o.PathRoot = pathRoot
	}
}

// WithFilepathSeparator will set the string value to join match patterns, defaults filepath.Separator
func WithFilepathSeparator(pathSeparator string) Option {
	return func(o *Options) {
		o.PathSeparator = pathSeparator
	}
}

// WithLocale is the string locale for the current match
func WithLocale(locale string) Option {
	return func(o *Options) {
		o.Locale = locale
	}
}

// WithFallbackLocale is the string locale to fallback if no Locale set or does not match
func WithFallbackLocale(fallbackLocale string) Option {
	return func(o *Options) {
		o.FallbackLocale = fallbackLocale
	}
}
