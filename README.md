```
     \__    __/
     /_/ /\ \_\
    __ \ \/ / __
    \_\_\/\/_/_/                 STENCIL
__/\___\_\/_/___/\__
  \/ __/_/\_\__ \/      A Templating package for Go
    /_/ /\/\ \_\
     __/ /\ \__
     \_\ \/ /_/
     /        \
```

# Stencil - A Templating package for Go

Templating in Go is not always trivial. This package has been designed to take a lot of the _"hurt"_ out of loading and retrieving
templates and nested templates by using smart name matching, with locales and sensible fallbacks.

If you like this package or are using for your own needs, then let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom)

## Features

- Well defined simple interfaces
- Easily extensible to suit your needs
- Fully tested
- Handles i18n/locales with fallback
- Cascading template match rules

## Installation

```
// Install with the usual or add to you package manager of choice.
go get -u gitlab.com/kylehqcom/stencil
```

Please feel free to use this package on its own merits. But if you want to see or use this package rendering html or templates with http responses, then I highly recommend using the [https://gitlab.com/kylehqcom/render](https://gitlab.com/kylehqcom/render) package directly which was born from, and leverages this package.

## Deeper dive

The **Stencil** package is built around x3 interfaces. The _Loader_, the _Matcher_ and the _Executor_. These x3 interfaces give you
all the flexibility to render your templates. Perhaps you need to Load a template from a Datastore on each request, use a custom matcher
to ensure you return the correct template from your loaded template collection, or execute your template with a side event on each
execute. This is all easily possible.

### The Loader interface

In the normal program flow, the _stencil.Loader.Load()_ method will parse the given template name and if successful, add to a `Loader.Collection` to return. The `stencil.Loader` has the options of `WithAllowDuplicates`, `WithLoadTimeout` and `WithMustParse`. It's recommended that you at least use the `WithMustParse` true when developing to surface errors early. Both allow and must options default false. LoadTimeout defaults to 5 seconds.

```go
// Loader is the interface used to "load" templates
Loader interface {

  // Load will load and parse your template source returning a Collection/error tuple
  Load() (*Collection, error)

  // RegisterFuncs is used to bind funcs to your templates prior to loading. As the
  // Load method parses your templates, it may be necessary to register placeholder funcs.
  // These can optionally be replaced at runtime as required and specified by the Go stdLib
  // template.FuncMap. Refer template.Funcs(funcMap FuncMap) *Template
  RegisterFuncs(template.FuncMap)
}

// Option defines the behaviours for loading
Option func(*Options)

// Options are all defined behaviours for loading
Options struct {

  // AllowDuplicates defines whether an error should be returned if duplicate found
  AllowDuplicates bool

  // LoadTimeout is used to return error on timeout duration is exceeded
  LoadTimeout time.Duration

  // MustParse if true, will panic on parse error
  MustParse bool
}
```

Of special note here is the `RegisterFuncs(template.FuncMap)` func. As we need to compile/parse templates prior to executing, you may need to RegisterFunc placeholders to ensure no errors on Load. Example

```go
// Layout.html contents
<!DOCTYPE html>
<html>
  <head>
    <title>Stencil</title>
  </head>
  <h1>Welcome to Stencil</h1>
  <body>
    {{render}}
  </body>
</html>

// Placeholder the render template func which will be replaced at execution time
var l = loader.NewFilepathLoader(loader.WithMustParse(true))
l.RegisterFuncs(template.FuncMap{"render": func() string {
  return "Render called unexpectedly"
}})
```

**Stencil** comes bundled with a `loader.Filepath` so you can take advantage of the `LoadFromFilepath(glob string)` func. This also takes care of the Go template.Templates.ParseGlob() func which does **NOT** respect files of the same name in sub directories and simply overwrites (last one wins).

A successful `Load()` call returns a non nil `*stencil.Collection` instance. From here, you can access the default Go `*template.Template` instance, and therefore all the default Go template funcs, through the `*stencil.Collection.T` field. A collection also utilises `sync.Mutex`. This is useful for parallel processing of large quantities of templates in goroutines.

### The Matcher interface

The `Matcher` is core to **Stencil** and is responsible for returning a correct Go \*template instance, using fallbacks if supplied, or error on not found. Default error templates can be assigned which also have fallbacks.

```go
// Matcher is the interface used to "match" templates
Matcher interface {

  // Match will match any pattern string you provide an return a template pointer or error
  Match(c *Collection, pattern string) (*template.Template, error)
}

// Options are all defined behaviours for matching
Options struct {

  // PathRoot if set, will be used to prefix all match patterns
  PathRoot string

  // PathSeparator is the Separator string used to join match patterns. Defaults to filepath.Separator
  PathSeparator string

  // Locale is the string for the current match
  Locale string

  // FallbackLocale is used if no match is made on the current Locale match
  FallbackLocale string
}
```

Again, **Stencil** comes bundled with a `matcher.Filepath` so matching template names is made easy. The matcher, based on the match options, will attempt to return a \*template.Template instance with the following cascading rules. Example

```go
m := matcher.NewFilepathMatcher(
  matcher.WithPathRoot("src/web/templates/"),
  matcher.WithLocale("es"),
  matcher.WithFallbackLocale("en"),
)

Calling m.match(c *Collection, "index.html") will check the following in order or preference

  1. src/web/templates/es/index.html
  2. src/web/templates/en/index.html
  3. src/web/templates/index.html
  4. index.html
```

The last match can be especially useful if you have a template loaded that has a name outside of your normal directory structure. This allows you to pass in a custom pattern and the matcher will lookup this value without any modifications.

### The Executor interface

The `Executor` in **Stencil** serves only as a simple abstraction and follows the Go \*template.Template.Execute() func. The abstraction ensures that this package is not bound to the core lib and it also gives you the freedom to manipulate the execution behaviour.

```go
// Executor is the interface used to "execute" templates
Executor interface {

  // Execute follows the Go packages Execute method for ease of use
  Execute(wr io.Writer, t *template.Template, data interface{}) error
}
```

### Next steps

From here my best advice would be to take a look at the [https://gitlab.com/kylehqcom/render](https://gitlab.com/kylehqcom/render) and the available examples. Using the `RenderResponse` instance which gives a good demonstration as to how to simply `Render` your templates.

If you have questions, praise or complaints, I am all ears so let me know via [https://twitter.com/kylehqcom](https://twitter.com/kylehqcom) for feedback. I use this package personally so I may have overlooked aspects outside of my domain. I do hope you find good use out of my efforts however =]
